var createError = require('http-errors');
const organizationsAccess = require('../data/organizations')

const getAll = async (req, res, next) => {
	const orgs = await organizationsAccess.getAllOrganizations()
	res.status(200).send(orgs)
}

const getByOrgCode = async (req, res, next) => {
	const org = await organizationsAccess.getOrganizationById(req.params.orgCode)
	if (org) {
		res.status(200).send({name:org.name, orgId: org.orgId});
	}else{
		next(createError(404));
	}
}

const addOrganization = async (req, res, next) => {
	try {
		await organizationsAccess.insertOrganization(req.body);
		res.status(201).send();
	} catch (e) {
		console.log(e);
		next(createError(400, 'Could not save new organization'));
	}
}

const editOrganization = async (req, res, next) => {
	try{
		await organizationsAccess.updateOrganization(req.body);
		res.status(200).send();
	}catch (e) {
		console.log(e);
		next(createError(400, 'Could not update organization'));
	}
}

module.exports = {
	getAll,
	getByOrgCode,
	addOrganization,
	editOrganization
}
