const express = require('express');
const router = express.Router();
const organizations = require('./organizations')

router
	.get('/organizations', organizations.getAll)
	.get('/organizations/:orgCode', organizations.getByOrgCode)
	.post('/organizations', organizations.addOrganization)
	.put('/organizations/:orgCode', organizations.editOrganization)


//some other router, same pattern


module.exports = router;
