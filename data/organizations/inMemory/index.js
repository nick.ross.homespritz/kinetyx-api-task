const orgModel = require('../organizationModel');

const __orgs__ = new Map();

const getAllOrganizations = () => {
	let orgs = [];
	__orgs__.forEach((value, key) => {
		orgs.push(value);
	});

	return Promise.resolve(orgs);
}

const insertOrganization = async (org) => {
	try {
		const current = await getOrganizationById(org.orgCode);
		if (current) return Promise.reject('An org by that code already exists');

		const newOrg = orgModel.build(org);
		newOrg.creationDate = new Date();
		__orgs__.set(newOrg.orgCode, newOrg);

		return Promise.resolve(1);
	} catch (e) {
		return Promise.reject(e);
	}
}

const getOrganizationById = (orgCode) => {
	return Promise.resolve(__orgs__.get(orgCode));
}

const updateOrganization = async (org) => {
	try {
		const current = await getOrganizationById(org.orgCode);
		if (!current) return Promise.reject('An org by that name doesnt exist');
		//TODO: merge here, though requirements do state to simply REPLACE.
		// This is the sort of behaviour that should be discussed in a real codebase
		const newOrg = orgModel.build(org);
		newOrg.creationDate = current.creationDate; // update shouldnt change creation date
		__orgs__.set(newOrg.orgCode, newOrg);

		return Promise.resolve(1);
	} catch (e) {
		return Promise.reject(e);
	}
}

module.exports = {
	getAllOrganizations,
	getOrganizationById,
	insertOrganization,
	updateOrganization
}
