const chai = require('chai');
const expect = chai.expect;

const orgModel = require('./organizationModel');

describe('Model Validation test', () => {
	describe('organization model', () => {
		it('validates correct organization model ', () => {
			const validOrg = {
				name: 'ABC Ing',
				orgCode: 'U23V',
				orgId: 'e17d0f3e-90af-458d-b304-5de9ef2e71d3',
				creationDate: '2020-12-09T06:34:13.588Z'
			}
			let actual = orgModel.build(validOrg);

			expect(validOrg).to.eql(actual)
		})

		it('validation fails organization model missing name', () => {
			const invalidOrg = {
				orgCode: 'U23V',
				orgId: 'e17d0f3e-90af-458d-b304-5de9ef2e71d3',
				creationDate: '2020-12-09T06:34:13.588Z'
			}
			expect(() => orgModel.build(invalidOrg)).to.throw('"name" is required')
		})

		//etc etc etc
	})
})
