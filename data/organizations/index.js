const {
	getAllOrganizations,
	getOrganizationById,
	insertOrganization,
	updateOrganization
} = require('./inMemory')
//Here every database provider would need to provide these methods
//And also here is where node is weak - type checking would be amazing to ensure
// that all data providers indeed satisfy the interface (which, thats what this is)
//I am only going to implement in memory storage if I am to get to the tests
// but I think the idea is demonstrated sufficiently

//ENV variable process.env.org_db_provider would be used to set up correct provider
// it is fed from the compose.yml file


module.exports = {
	getAllOrganizations,
	getOrganizationById,
	insertOrganization,
	updateOrganization
}
