const Joi = require('joi');

const schema = Joi.object({
	name: Joi.string().required(),
	orgId: Joi.string().required().guid(),
	orgCode: Joi.string().required(),
	creationDate: Joi.string().allow(null, '')//dates are a pain, would need to convert to date
	//and then get it as a .raw(). using string to simplify it for now
});

const build = (org = {}) => {
	const result = schema.validate(org);
	if (result.error){
		let errors = result.error.details.map(err => err.message).join('\n')
		throw new Error(errors);
	}
	return {...org}
}

module.exports = {
	build
}
